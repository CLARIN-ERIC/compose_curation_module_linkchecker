#!/usr/bin/env bash
BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

CURATION_COMPOSE_DIR="${BASE_DIR}/clarin"


COMPOSE_OPTS="${COMPOSE_OPTS}"
COMPOSE_CMD_ARGS="${COMPOSE_CMD_ARGS}"

start_curation_linkchecker_main() {
	create_missing_volume "vlo-data"		
	create_missing_network "network_linkchecker" "--internal"

	_docker-compose ${COMPOSE_OPTS} up ${COMPOSE_CMD_ARGS}
}

create_missing_volume() {
	VOLUME="$1"
	shift
	OPTIONS="$*"
	if [ "${VOLUME}" ] && [ "$(docker volume ls |grep -c "${VOLUME}")"  -eq 0 ]; then	
		echo "Creating missing volume '${VOLUME}'"
		if [ "${OPTIONS}" ]  && [ "${#OPTIONS[@]}" ]; then
			docker volume create "${OPTIONS[@]}" -- "${VOLUME}"
		else
	        docker volume create -- "${VOLUME}"
	    fi
    fi

}

create_missing_network() {
	NETWORK="$1"
	shift
	OPTIONS="$*"
	if [ "${NETWORK}" ] && [ "$(docker network ls |grep -c "${NETWORK}")"  -eq 0 ]; then
		echo "Creating missing network '${NETWORK}'"
		if [ "${OPTIONS}" ]  && [ "${#OPTIONS[@]}" ]; then
	        docker network create "${OPTIONS[@]}" -- "${NETWORK}"
	    else
		    docker network create -- "${NETWORK}"
		fi
    fi
}

_docker-compose() {
	(cd "${CURATION_COMPOSE_DIR}" && docker-compose "$@")
}

start_curation_linkchecker_main "$@"
