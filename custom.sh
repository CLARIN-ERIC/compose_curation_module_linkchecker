#!/usr/bin/env bash
BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
CURATION_COMPOSE_DIR="${BASE_DIR}/clarin"

sub_help(){
    echo "Usage: ${PROGRAM_NAME} <subcommand> [options]"
    echo ""
    echo "Subcommands:"
    echo "    start-link-checking     Start the link checking process inside the linkchecker container"
    echo "    stop-link-checking      Stop the link checking process inside the linkchecker container" 
    echo "    status-link-checking     Output information about the status of the link checker (running or not)" 
    echo "    start-update-dashboard  Start reports generation in curation-module container" 
    echo "    stop-container          Stop only given container(if there are not dependant containers)."
    echo "                            Possible container names are: curation-module, linkchecker, mysql"
    echo ""
}

sub_stop-container() {
    #if check_service; then
        _docker-compose stop "$1"
    #else
    #    echo "Service not running, cannot execute import."
    #    exit 1
    #fi
}

sub_status-link-checking() {
   STATUS=$( _docker-compose exec -T linkchecker bash -c '/app/storm/bin/storm list'|grep linkchecker|awk '{print $2}' )
   if [ "${STATUS}" = 'ACTIVE' ]; then
      IS_RUNNING='true'
   else
      IS_RUNNING='false'   	  
   fi
   
   echo "{'linkchecker_running': ${IS_RUNNING}}"
}

sub_start-link-checking() {
   status=$( _docker-compose exec -T linkchecker bash -c '${STORM_DIRECTORY}/bin/storm list'|grep linkchecker|awk '{print $2}' )
   if [ -z "$status"]; then
      _docker-compose exec -T linkchecker bash -c '${STORM_DIRECTORY}/bin/storm jar ${LINKCHECKER_DIRECTORY}/linkchecker.jar org.apache.storm.flux.Flux -e -r -R ${LINKCHECKER_FLUX_FILE}'
   elif [ $status = 'INACTIVE' ]; then
      _docker-compose exec -T linkchecker bash -c '${STORM_DIRECTORY}/bin/storm activate linkchecker'
   else
      echo "the link checker is already running..."
   fi
}

sub_stop-link-checking() {
    _docker-compose exec -T linkchecker bash -c '${STORM_DIRECTORY}/bin/storm kill -w 0 linkchecker'
}

sub_start-update-dashboard() {
   status=$( _docker-compose exec -T curation-dashboard bash -c 'supervisorctl -u sysops -p $SUPERVISOR_PASSWORD status curation-app'|awk '{print $2}' )
   if [ "$status" != 'RUNNING' ]; then
      _docker-compose exec -T -d curation-dashboard bash -c 'supervisorctl -u sysops -p $SUPERVISOR_PASSWORD start curation-app'
   else
      echo "the dashboard update is already running..."
   fi
}


_docker-compose() {
    (cd "${CURATION_COMPOSE_DIR}" && docker-compose "$@")
}

#
# Process subcommands
#
subcommand=$1
case $subcommand in
    "" | "-h" | "--help")
        sub_help
        ;;
    *)
        shift
        sub_${subcommand} $@
        if [ $? = 127 ]; then
            echo "Error: '${subcommand}' is not a known subcommand." >&2
            echo "       Run '${PROGRAM_NAME} --help' for a list of known subcommands." >&2
            exit 1
        fi
        ;;
esac
