# CLARIN Deployment of Curation Dashboard and Link Checker

This project is responsible for the deployment, running and the maintenance of the Curation Dashboard and the Link Checker projects.
It consists of a Docker-compose project, which consists of 3 Docker containers: `Curation Module`, `Linkchecker`, `MySQL`. If you are reading this,
it means you will be probably developing and maintaining `Curation Module` and `Linkchecker` projects. The `MySQL` service is using the [docker-mariadb](https://gitlab.com/CLARIN-ERIC/docker-mariadb/) image maintained by Clarin admins and this project uses an image that they provide.

## Curation Module Project
The Curation Module codebase can be found [here](https://github.com/clarin-eric/clarin-curation-module). The Curation Module Clarin Deployment project is called [docker-curation-module](https://gitlab.com/CLARIN-ERIC/docker-curation-module).
This docker-compose project uses the image created by the `docker-curation-module` project.

## Linkchecker Project
The Linkchecker codebase can be found [here](https://github.com/clarin-eric/linkchecker). The Linkchecker Clarin Deployment project is called
[docker-linkchecker](https://gitlab.com/CLARIN-ERIC/docker-linkchecker).
This docker-compose project uses the image created by the `docker-linkchecker` project.

## Running and operation

After deployment (see below) and initial configuration, the project can be controlled using the [CLARIN control script](https://gitlab.com/CLARIN-ERIC/control-script/). In addition to the default commands to start/stop all services etc, there are several important subcommands:

* `start-link-checking` triggers the start or resumption of the link checking process in the `curation-module` container
* `stop-link-checking` causes the link checking process in the `curation-module` container to be stopped/paused
* `start-update-dashboard` triggers the start of an update of the Curation Dashboard (generation of new reports that form the web app content)

For an complete overview and further descriptions of the actual subcommands, type `bash control.sh -s --help`

## Building and deploying

### Building the images
You can build the docker-curation-module project with this command: `./build.sh --local --build`.

**copy_data.sh:** This script is run before the Docker image is built. 
Therefore, it either clones the codebase from the repository or 
copies it from where it is specified locally. There are two files which determine where the
codebase comes from:

1. `copy_data.env.sh`(default): This file contains the repository url and the tag to be downloaded.
2. `copy_data_dev.env.sh`: This file contains the location of your local files.

If you want to build locally from your local files the build command needs to be run like this: `DATA_ENV_FILE=copy_data_dev.env.sh ./build.sh --local --build`

**Init folder:** When you want a script to be run at the start of the docker container, they should be copied (through the Dockerfile) into `/init` folder inside the container. Name of the script doesn't matter, all the scripts inside will be run.

**Maven:** Maven downloads the dependencies every time during building. To save time, I created a folder: `docker-curation-module/image/maven_repository`. 
To fill this folder you can run in the codebase: `mvn -Dmaven.repo.local=.../docker-curation-module/image/maven_repository clean install`. This folder is then used during the building of the image from the Dockerfile, so that all the dependencies are not downloaded every time.
TODO: Same thing should be done for `docker-linkchecker` project. 

**Dockerfile:** Base images are maintained by Clarin admins, which are based on Alpine.

### Configuration and deployment of the `compose_curation_module_linkchecker` project

**Environment and Config Variables:** Environment Variables are defined in the `.env` file. 
However, since there can be secrets in the `.env` file, in the repository only `.env-template` 
file is saved. This file should contain default values for all the variables and shouldn't 
contain real passwords. You need to change the `.env` file manually in the server and add 
the passwords there.

Config files for projects also shouldn't contain any secret passwords. They will be set in the production 
environment through environment variables. Here is how to set it up: The environment from `.env` file
are used in the `docker-compose.yml` to forward them into the containers. Now that the variables are
available inside the containers as environment variables, an init script is needed to put them into actual
config files needed by the application. For curation module, the init script can be found [here](https://gitlab.com/CLARIN-ERIC/docker-curation-module/-/blob/master/image/init.sh).
This script utilizes a `replaceVarInFile` command available in the base image to replace placeholders in config.properties file with environment variables.

**Mounts and Logs:** All three containers have mounted volumes for data persitence. Logs are persisted through a mount as well, but the preferred way of capturing and storing logs is through fluentd. The images have fluentd configurations that define log sources. These are automatically captured and aggregated into a single combined docker output. On the server environments, these are collected and sent to CLARIN's central log aggregation endpoint that can be accessed through [logs.clarin.eu](https://logs.clarin.eu) (access can be granted by the CLARIN system administrators).

#### Clarin Deployment Tutorial
Can:
> The tutorial which I learned how things work from can be found [here](https://docs.google.com/document/d/1PjIyIUqYbBlpjWpHKljYvzqlqmpI0_yk9RuwAqE253M/edit). If you need inspiration for features, which this repository doesn't have, a good example to look at is the [compose VLO Project](https://gitlab.com/CLARIN-ERIC/compose_vlo) and the [docker VLO Project](https://gitlab.com/CLARIN-ERIC/docker-vlo-beta).
It should be mentioned that some things might be outdated. Also, I needed a lot of extra help from Clarin side to understand everything. So don't be shy to ask. 

## Server and Users
Currently, the project is deployed on the server `hetzner-vps3`. The server is managed by Clarin admins, who have root access to the server. 
They should create a user for you with necessary rights, which won't be much outside of very basic commands (ex: docker ps). What your user can run or not are defined in a list which is managed by the admins. 
If you need more rights, contact the server admins.
 
There is a user called `deploy` on the server which needs to be used to perform deployment operations. Deployment operations are all supplied via scripts. 
`control.sh`, `deploy.sh`, `build.sh` are the most important scripts. Usage of these scripts can be seen below in the "Common use case: Deploying after changing code" section. 
The projects are deployed into the home folder of `deploy` user. Examples of using `deploy` user will be explained further below.

There is also the possibility to add your own commands to `control.sh`. Simply add them into `custom.sh` in the root of the project. An example is stop-container.
If a sub-command is not defined in the `control.sh`, it will be delegated to `custom.sh`. 
Example: `bash control.sh -s stop-container linkchecker`

## Gitlab CI and Docker Registry
The repositories are all hosted on public Gitlab and the images are hosted on Clarin's private Docker Registry. The provided scripts and gitlab ci runners take care of tagging docker images (after git tags).
`.gitlab-ci.yml` file for each project is used to define what is executed by the runners, which should invlolve build and uploading to docker registry.
Git tags should be used to trigger a push of the image to the docker registry. In the `docker-linkchecker` or `docker-curation-module` project, run:

1. `git tag -a 1.0.0 -m "update versions"`
2. `git push origin 1.0.0`

After this, a gitlab pipeline will start, which if successful will push the image with the same tag to the registry.  

## Common use case: Deploying after changing code
1. Implement a new feature in Curation Module code base, afterwards run:
    1. `git add --all`
    2. `git commit -m "implement new feature"`
    3. `git tag -a {version_tag1} -m "implement new feature"`
    4. `git push origin {version_tag1}`
2. In the docker-curation-module project update the `CURATION_BRANCH` to {version_tag1} in the `copy_data.env.sh` file, then run: 
    1. `git commit -m "update cm version" copy_data.env.sh` 
    2. `git tag -a {version_tag2} -m "update cm version"`
    3. `git push origin {version_tag2}` 
    
    This will trigger a pipeline on Gitlab for building and pushing to docker registry.
3. In this project, change the curation module image tag to {version_tag2} in `docker-compose.yml` file. Then run:
    1. `git commit -m "update cm image version" docker-compose.yml`
    2. `git tag -a {version_tag3} -m "update cm image version"`
    3. `git push origin {version_tag3}` 
4. Go into the server and navigate to `/home/deploy`
    1. Make sure the Gitlab Pipeline from step 2 is finished.
    2. Pull image with: `bash ./deploy.sh --name curation-module-linkchecker --git compose_curation_module_linkchecker --tag {version_tag3}`
    3. Deploy the project with: `bash ./control.sh curation-module-linkchecker start`
    
    The deployment can then be stopped with: `bash ./control.sh curation-module-linkchecker stop`
    
    If you wish to stop only one container: `bash ./control.sh curation-module-linkchecker stop-container {container_name}`
    
    If you wish to get inside a container: `docker exec -it curation_{container_name}_1 bash`


## Monitoring & Logging
Base Images include supervisor and fluentd which automatically aggregate logs for Clarin admins to view. There is currently no `icinga` like monitoring set up, which needs to done.


